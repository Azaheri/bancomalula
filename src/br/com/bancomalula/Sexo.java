package br.com.bancomalula;

public enum Sexo {
	MASCULINO("Masculino"),
	FEMININO("Feminino"),
	OUTRO("Outro");
	
	public String nomeDoSexo;
	
	Sexo(String nomeDoSexo) {
		this.nomeDoSexo = nomeDoSexo;
	}
}
