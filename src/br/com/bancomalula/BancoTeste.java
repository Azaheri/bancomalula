package br.com.bancomalula;

/**
 * 
 * @author Matheus Jos�
 *
 */

public class BancoTeste {

	public static void main(String[] args) {
		
		Conta.saldoDoBanco = 2_000_000.00;
		
		ContaCorrente cc = new ContaCorrente(1, 500, "123!", new Cliente("Matheus", "468468448", "486486515798", "sadui@msadju.com", Sexo.MASCULINO));
		
		ContaPoupanca cp = new ContaPoupanca(1, 100, "456!", new Cliente("Matheus", "464165415", "46841686849", "dsadsa@sad.com", Sexo.MASCULINO));
		
		System.out.println("Dados da conta");
		System.out.println("N�            " + cc.getNumero());
		System.out.println("Saldo:        " + cc.getSaldo());
		System.out.println("Senha:        " + cc.getSenha());
		System.out.println("Titular:      " + cc.getCliente().getNome());
		System.out.println("CPF:          " + cc.getCliente().getCpf());
		System.out.println("RG:           " + cc.getCliente().getRg());
		System.out.println("E-mail:       " + cc.getCliente().getEmail());
		System.out.println("Sexo:         " + cc.getCliente().getSexo().nomeDoSexo);
		
		
//		Conta conta = new Conta(1, "Corrente", 29_985.99, "123!", new Cliente("Matheus", "2313214", "4213213421", "matheus@email.com", Sexo.MASCULINO));
//		
//		
//		conta.exibeSaldo();
//		conta.deposita(50);
//		conta.exibeSaldo();
//		System.out.println("Cofre: " + Conta.saldoDoBanco);
//		conta.saca(100);
//		conta.exibeSaldo();
//		System.out.println("Cofre: " + Conta.saldoDoBanco);
//		
//		System.out.println();
//		System.out.println("=============================================");
//		System.out.println();
//		
//		//mostrar os dados da conta
//		System.out.println("Dados da conta:");
//		System.out.println("N�             " + conta.getNumero());
//		System.out.println("Tipo:          " + conta.getTipo());
//		System.out.println("Senha:         " + conta.getSenha());
//		System.out.println("Saldo:         " + conta.getSaldo());
//		System.out.println("Titular:       " + conta.getCliente().getNome());
//		System.out.println("CPF:           " + conta.getCliente().getCpf());
//		System.out.println("RG:            " + conta.getCliente().getRg());
//		System.out.println("E-mail:        " + conta.getCliente().getEmail());
//		System.out.println("Sexo:          " + conta.getCliente().getSexo().nomeDoSexo);
	}

}
