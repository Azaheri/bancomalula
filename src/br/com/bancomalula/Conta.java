package br.com.bancomalula;

/**
 * 
 * @author Matheus Jos�
 *
 */

public class Conta {
	//cofre do banco, toda vez que algu�m saca ou deposita esse cofre sofre a altera��o
		public static double saldoDoBanco;
	
	//atributos	
		private int numero;
		private double saldo;
		private String senha;
		private Cliente cliente;
		
	//construtor
		public Conta(int numero, double saldo, String senha, Cliente cliente) {
			this.numero = numero;
			this.saldo = saldo;
			this.senha = senha;
			this.cliente = cliente;
		}
		
	//get/set
		public int getNumero() {
			return numero;
		}

		public void setNumero(int numero) {
			this.numero = numero;
		}

		public double getSaldo() {
			return saldo;
		}

		public void setSaldo(double saldo) {
			this.saldo = saldo;
		}

		public String getSenha() {
			return senha;
		}

		public void setSenha(String senha) {
			this.senha = senha;
		}

		public Cliente getCliente() {
			return cliente;
		}

		public void setCliente(Cliente cliente) {
			this.cliente = cliente;
		}
		
	//m�todos
		public void exibeSaldo() {
			System.out.println(cliente.getNome() + " seu saldo � de R$ "  + this.getSaldo());
		}
		
		public void saca(double valor) {
			this.saldo -= valor;
			Conta.saldoDoBanco -= valor;
		}
		
		public void deposita(double valor) {
			this.saldo += valor;
			Conta.saldoDoBanco += valor;
		}
		
		public void transferePara(Conta destino, double valor) {
			this.saca(valor);
			destino.deposita(valor);			
		}
}
